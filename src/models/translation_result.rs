use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct TranslationResults {
    pub translations: Vec<TranslationResult>
}

#[derive(Debug, Deserialize, Clone)]
pub struct TranslationResult {
    pub detected_source_language: String,
    pub text: String
}
