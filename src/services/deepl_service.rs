use isahc::{prelude::*, HttpClient};
use form_urlencoded::Serializer;

use crate::models::translation_result::{TranslationResults, TranslationResult};
use crate::models::error::Error;

pub struct DeeplService {
    client: HttpClient,
    api_key: Option<String>
}

impl DeeplService {
    const BASE_URL: &'static str = "https://api.deepl.com/v2/";

    pub fn new(api_key: Option<String>) -> DeeplService {
        let client = HttpClient::new()
            .unwrap_or_else(|_| panic!("Unable to initialize http client."));

        DeeplService {
            client,
            api_key
        }
    }

    pub fn translate(&self, text: &str, source_lang_code: &str, target_lang_code: &str) -> Result<Option<TranslationResult>, Error> {
        let api_key = self.api_key.as_ref().ok_or(Error::NoApiKey)?;

        let params = Serializer::new(String::new())
            .append_pair("auth_key", api_key)
            .append_pair("text", text)
            .append_pair("source_lang", source_lang_code)
            .append_pair("target_lang", target_lang_code)
            .finish();

        let mut response = self.client.get(format!("{}{}?{}", DeeplService::BASE_URL, "translate", params))?;
        let translation_results: TranslationResults = serde_json::from_str(&response.text()?)?;
        let translations = translation_results.translations;
        let t = translations[0].clone();

        Ok(Some(t))
    }
}
