use gtk::prelude::BuilderExtManual;
use gtk::{WidgetExt, StackExt, ButtonExt, BoxExt, LabelExt, CssProviderExt, StyleContext, TextBufferExt};
use gtk::StackTransitionType::SlideLeftRight;
use glib::signal::Inhibit;
use crate::models::language::Language;
use std::collections::HashMap;
use crate::state::State;
use std::rc::Rc;

pub struct MainWindow {
    window: gtk::Window,
    main_stack: gtk::Stack,
    translate_button: gtk::Button,
    menu_button: gtk::MenuButton,
    back_button: gtk::Button,
    star_button: gtk::Button,
    source_language_label1: gtk::Label,
    source_language_label2: gtk::Label,
    target_language_label1: gtk::Label,
    target_language_label2: gtk::Label,
    source_text_buffer: gtk::TextBuffer,
    target_text_buffer: gtk::TextBuffer,
    source_language_buttons: HashMap<String, gtk::ModelButton>,
    target_language_buttons: HashMap<String, gtk::ModelButton>,
}

impl MainWindow {

    pub fn new(state: &State) -> MainWindow {
        let template_str = include_str!("../../resources/ui/main_window.glade");
        let builder = gtk::Builder::from_string(template_str);

        let style_str = include_str!("../../resources/ui/style.css");
        let style_provider = gtk::CssProvider::new();
        style_provider.load_from_data(style_str.as_bytes()).expect("Failed to load css style.");
        StyleContext::add_provider_for_screen(
            &gdk::Screen::get_default().expect("Error initializing gtk css provider."),
            &style_provider,
            gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
        );

        let window: gtk::Window = builder.get_object("mainWindow").unwrap();
        let main_stack: gtk::Stack = builder.get_object("mainStack").unwrap();
        let translate_button: gtk::Button = builder.get_object("translateButton").unwrap();
        let menu_button: gtk::MenuButton = builder.get_object("menuButton").unwrap();
        let back_button: gtk::Button = builder.get_object("backButton").unwrap();
        let star_button: gtk::Button = builder.get_object("starButton").unwrap();
        let source_language_label1: gtk::Label = builder.get_object("sourceLanguageLabel1").unwrap();
        let source_language_label2: gtk::Label = builder.get_object("sourceLanguageLabel2").unwrap();
        let target_language_label1: gtk::Label = builder.get_object("targetLanguageLabel1").unwrap();
        let target_language_label2: gtk::Label = builder.get_object("targetLanguageLabel2").unwrap();
        let source_languages_box: gtk::Box = builder.get_object("sourceLanguagesBox").unwrap();
        let target_languages_box: gtk::Box = builder.get_object("targetLanguagesBox").unwrap();
        let source_text_buffer: gtk::TextBuffer = builder.get_object("sourceTextBuffer").unwrap();
        let target_text_buffer: gtk::TextBuffer = builder.get_object("targetTextBuffer").unwrap();
        let mut source_language_buttons = HashMap::new();
        let mut target_language_buttons = HashMap::new();

        let source_languages = &state.source_languages;
        let target_languages = &state.target_languages;

        for language in source_languages {
            MainWindow::add_language_button(&language, &source_languages_box, &mut source_language_buttons);
        }
        for language in target_languages {
            MainWindow::add_language_button(&language, &target_languages_box, &mut target_language_buttons);
        }

        source_languages_box.show_all();
        target_languages_box.show_all();
        translate_button.set_sensitive(false);

        let window = MainWindow {
            window,
            main_stack,
            translate_button,
            menu_button,
            back_button,
            star_button,
            source_language_label1,
            source_language_label2,
            target_language_label1,
            target_language_label2,
            source_text_buffer,
            target_text_buffer,
            source_language_buttons,
            target_language_buttons
        };
        window.update_from(state);

        window
    }

    fn add_language_button(language: &Language, button_box: &gtk::Box, button_map: &mut HashMap<String, gtk::ModelButton>) {
        let button: gtk::ModelButton = gtk::ModelButton::new();
        button.set_property_text(Option::Some(&language.name));

        button_box.pack_start(&button, false, true, 0);
        button_map.insert(language.code.clone(), button);
    }

    pub fn get_source_text(&self) -> String {
        let text = self.source_text_buffer.get_text(&self.source_text_buffer.get_start_iter(), &self.source_text_buffer.get_end_iter(), true);

        match text {
            Some(text) => text.as_str().to_string(),
            None => String::new()
        }
    }

    pub fn start(&self) {
        glib::set_application_name("Interpret");
        self.window.connect_delete_event(|_, _| { gtk::main_quit(); Inhibit(false) });
        self.window.show_all();
    }

    pub fn switch_to_translation_page(&self) {
        self.main_stack.set_visible_child_full("translationPage", SlideLeftRight);
        self.menu_button.set_visible(true);
        self.back_button.set_visible(false);
        self.star_button.set_visible(false);
    }

    pub fn switch_to_result_page(&self) {
        self.main_stack.set_visible_child_full("resultPage", SlideLeftRight);
        self.menu_button.set_visible(false);
        self.back_button.set_visible(true);
        self.star_button.set_visible(true);
    }

    pub fn on_translate_button_click<F: Fn(&gtk::Button) + 'static>(&self, callback: F) {
        self.translate_button.connect_clicked(callback);
    }

    pub fn on_back_button_click<F: Fn(&gtk::Button) + 'static>(&self, callback: F) {
        self.back_button.connect_clicked(callback);
    }

    pub fn on_source_language_button_click<F: Fn(&str) + 'static + ?Sized>(&self, callback: Rc<F>) {
        for (language_code, button) in self.source_language_buttons.clone() {
            let callback = Rc::clone(&callback);
            button.connect_clicked(move |_| callback(&language_code));
        }
    }

    pub fn on_target_language_button_click<F: Fn(&str) + 'static + ?Sized>(&self, callback: Rc<F>) {
        for (language_code, button) in self.target_language_buttons.clone() {
            let callback = Rc::clone(&callback);
            button.connect_clicked(move |_| callback(&language_code));
        }
    }

    pub fn on_source_text_buffer_changed<F: Fn(&gtk::TextBuffer) + 'static>(&self, callback: F) {
        self.source_text_buffer.connect_changed(callback);
    }

    pub fn update_from(&self, state: &State) {
        let selected_source_language = state.source_languages
            .iter()
            .find(|l| l.code == state.selected_source_language_code)
            .unwrap();
        let selected_target_language = state.target_languages
            .iter()
            .find(|l| l.code == state.selected_target_language_code)
            .unwrap();

        self.source_language_label1.set_label(&selected_source_language.name);
        self.source_language_label2.set_label(&selected_source_language.name);
        self.target_language_label1.set_label(&selected_target_language.name);
        self.target_language_label2.set_label(&selected_target_language.name);
        self.target_text_buffer.set_text(&state.target_text);

        match &state.source_text.chars().count() > &0 {
            true => self.translate_button.set_sensitive(true),
            false => self.translate_button.set_sensitive(false)
        }
    }

}
